apache
==========

Ansible playbook that installs and configures Apache2 with modules on Debian

Role Variables
--------------

The desired behavior can be refined via variables.

Option | Description
--- | ---
`apache_packages` | A list of apache packages to install.
`apache_listen_port` | Port apache listens in vhost (default `80`)
`apache_modules` | A list of apache modules to install and enable.
`apache_default_vhost_filename` | Name of the default vhost file (000-default.conf)
`apache_remove_default_vhost` | Should the default vhost be removed (default `false`)
`apache_server_tokens_prod` | Disable apache server tokens (default `true`)
`apache_server_signature_off` | Disable apache server signature (default `true`)


The following `apache_packages` are defined by default:

```yaml
  - apache2
  - apache2-utils
```

The following `apache_modules` are defined by default:

```yaml
  - actions
  - alias
  - headers
  - rewrite
  - ssl
```

Example Playbook
----------------

The following will install wget and update all packages on your local host.

```yaml
# file: test.yml
- hosts: local

  vars:
    apache_remove_default_vhost: true
        
  roles:
    - apache
```